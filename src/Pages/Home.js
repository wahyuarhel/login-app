import React from "react";

import { checkLogin } from "../Helper";

export default function Home() {
  return (
    <div>
      <div>{checkLogin() ? "Sudah Login " : "Belum Login"}</div>
    </div>
  );
}
